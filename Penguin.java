import java.util.Random;

public class Penguin
{
	public String type;
	public String name;
	public int age;
	public char gender;
	
	int fishCaught;
	boolean isLeading;
	
	
	
	public void penguinStats()
	{
		System.out.println("Name: " + this.name + "\nType: " + this.type + "\nAge: " + this.age + "\nSex: " + this.gender);
	}
	
	public void hunt()
	{
		
		if(this.age < 2)
		{
			System.out.println(this.name + " can not hunt for they are too young");
		}
		else
		{
			Random rand = new Random();
			if(rand.nextInt(5) == 4)
			{
				System.out.println(this.name + " could not catch a fish.");
			}
			else
			{
				System.out.println(this.name + " caught a fish!");
				fishCaught++;
			}
		}
	}
	
	public void lead()
	{
		if(this.age >= 4 && this.type.equals("Emperor"))
		{
			if(!this.isLeading)
			{
				System.out.println(this.name + " is now leading.");
				this.isLeading = true;
			}
			else if(this.isLeading)
			{
				System.out.println(this.name + " is no longer leading.");
				this.isLeading = false;
			}
		}
		else
		{
			System.out.println(this.name + " can not lead.");
		}
	}
	
	public void caughtFish()
	{
		System.out.println(this.name + " caught: " + this.fishCaught + " fish.");
	}
}