import java.util.Scanner;

public class NationalPark
{
	public static void main(String[] args)
	{
		Penguin[] waddle = new Penguin[4];
		Scanner scan = new Scanner(System.in);
		
		for(int i = 0; i < waddle.length; i++)
		{
			waddle[i] = new Penguin();
			System.out.println("Enter the penguin's name");
			waddle[i].name = scan.next();
			System.out.println("Enter the penguin's type");
			waddle[i].type = scan.next();
			System.out.println("Enter the penguin's age");
			waddle[i].age = scan.nextInt();
			System.out.println("Enter the penguin's gender(Male/Female)");
			String tempGender = scan.next();
			waddle[i].gender = tempGender.charAt(0);
		}
		
		waddle[1].penguinStats();
		System.out.println(" ");
		waddle[2].penguinStats();
		System.out.println(" ");
		waddle[3].penguinStats();
		System.out.println(" ");
		
		waddle[0].hunt();
		System.out.println(" ");
		waddle[0].hunt();
		System.out.println(" ");
		waddle[0].lead();
		System.out.println(" ");
		waddle[0].lead();
		System.out.println(" ");
		waddle[0].caughtFish();
	}
}
		